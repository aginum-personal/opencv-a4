import cv2
import numpy as np
import argparse
import math

A4_HEIGHT = 297.0
A4_WIDTH = 210.0


def view_image(image, name_of_window):
    cv2.namedWindow(name_of_window, cv2.WINDOW_AUTOSIZE)

    scale_percent = 20  # percent of original size
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

    cv2.imshow(name_of_window, resized)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def order_points(pts):
    # sort contour points clockwise starting from upper-left
    rect = np.zeros((4, 2), dtype="float32")
    # the top-left point will have the smallest X-Y sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # top-right point will have the smallest X-Y difference,
    # the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect.astype('int32')
    # TODO: potential problems with rotated sheets of paper / extreme perspective


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Detects sheet of A4 paper, calculates distance from camera to it and orientation of camera relative to it')
    parser.add_argument('-f','--filename')
    args = parser.parse_args()
    if args.filename:
        img = cv2.imread(args.filename)
    else:
        # img = cv2.imread(r'a4-test/IMG_20201125_094433.jpg')  # white on white, bad
        # img = cv2.imread(r'a4-test/IMG_20201125_094439.jpg')  # white on white, bad
        # img = cv2.imread(r'a4-test/IMG_20201125_094450.jpg')  # fine
        # img = cv2.imread(r'a4-test/IMG_20201125_094500.jpg')  # white on black, extremely rotated: fine detection, but point order seems odd
        img = cv2.imread(r'a4-test/IMG_20201125_094527.jpg')  # messy background, fine
        # img = cv2.imread(r'a4-test/IMG_20201125_094540.jpg')  # white on white, bad

        # img = cv2.imread(r'a4-test/IMG_20201125_231312.jpg')  # straight
        # img = cv2.imread(r'a4-test/IMG_20201125_231327.jpg')  # to front
        # img = cv2.imread(r'a4-test/IMG_20201125_231337.jpg') # to left
        # img = cv2.imread(r'a4-test/IMG_20201125_231347.jpg')  # rotated counterclockwise
        # img = cv2.imread(r'a4-test/IMG_20201125_235252.jpg')  # ~300mm to paper
        # img = cv2.imread(r'a4-test/IMG_20201126_083445.jpg')  # extreme angle, not great

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    # gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # gray = cv2.bilateralFilter(gray, 9, 75, 75)
    # view_image(gray, "filtered")

    thresholded = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)
    thresholded = cv2.medianBlur(thresholded, 31)

    # view_image(thresholded, "thresholded")

    edged = cv2.Canny(thresholded, 100, 255)
    # view_image(edged, "Canny")

    # find the contours in the edged image and keep the largest one
    # assuming that this is our piece of paper in the image
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]

    # sort and choose the largest contour
    cnts = sorted(cnts, key=cv2.contourArea)
    cnt = cnts[-1]

    # approx the contour, so the get the corner points
    arclen = cv2.arcLength(cnt, True)
    approx = cv2.approxPolyDP(cnt, 0.02 * arclen, True)

    canvas = img.copy()
    cv2.drawContours(canvas, [cnt], -1, (255, 0, 0), 10, cv2.LINE_AA)
    cv2.drawContours(canvas, [approx], -1, (0, 0, 255), 10, cv2.LINE_AA)

    # view_image(canvas, "Contours")

    print(f"contour points: {approx.shape[0]}")
    if approx.shape[0] != 4:
        print("valid quadrangle is not detected")
        exit(-1)

    approx = approx.reshape(4, 2)
    approx = order_points(approx)

    image_points = approx.astype(np.float)

    # vertical
    world_points_vertical = np.array([
        (0.0, 0.0, 0.0),  # top-left
        (A4_WIDTH, 0.0, 0.0),  # top-right
        (A4_WIDTH, A4_HEIGHT, 0.0),  # bottom-right
        (0.0, A4_HEIGHT, 0.0)  # bottom-left
    ], dtype=np.float)

    # horizontal
    world_points_horizontal = np.array([
        (0.0, 0.0, 0.0),  # top-left
        (A4_HEIGHT, 0.0, 0.0),  # top-right
        (A4_HEIGHT, A4_WIDTH, 0.0),  # bottom-right
        (0.0, A4_WIDTH, 0.0)  # bottom-left
    ], dtype=np.float)

    height_left = math.sqrt((approx[3][0]-approx[0][0])**2+(approx[3][1]-approx[0][1])**2)
    height_right = math.sqrt((approx[2][0]-approx[1][0])**2+(approx[2][1]-approx[1][1])**2)
    width_top = math.sqrt((approx[1][0]-approx[0][0])**2+(approx[1][1]-approx[0][1])**2)
    width_bottom = math.sqrt((approx[3][0]-approx[2][0])**2+(approx[3][1]-approx[2][1])**2)

    # try to determine sheet orientation on image: error possible with extreme perspective
    if (height_left + height_right) > (width_top + width_bottom):
        world_points = world_points_vertical
        print("Sheet orientation: vertical")
    else:
        world_points = world_points_horizontal
        print("Sheet orientation: horizontal")
    # Camera internals
    size = img.shape
    focal_length = size[1]  # naive focal length approximation

    # as we don't have camera calibration, center of the sensor is assumed to be in the center of image
    center = (size[1] / 2, size[0] / 2)

    camera_matrix = np.array(
        [[focal_length, 0, center[0]],
         [0, focal_length, center[1]],
         [0, 0, 1]], dtype="double"
    )

    dist_coeffs = np.zeros((4, 1))  # lens distortion ignored as we dont have calibration

    # [0,0,0] real world coordinates in Perspective-n-Point problem refers to camera center
    # so, assigning [0, 0, 0] point to point in space (upper-left corner of the piece of paper here)
    # camera position and orientation relative to this point could be obtained automatically
    # TODO (only directions of movements should be inverted (should they?))
    (success, rotation_vector, translation_vector) = cv2.solvePnP(world_points, image_points, camera_matrix,
                                                                  dist_coeffs)


    # camera position relative to upper-left corner of paper piece
    # positive direction of Z axis points forward, X - rightwards, Y - down  (in sheet plane)
    # positive rotation - clockwise (as seen from coordinate system origin)
    print("Camera Rotation Vector (X, Y, Z, deg):\n {0}".format(-rotation_vector * 180 / np.pi))  # in degrees
    print("Camera Translation Vector (X, Y, Z, mm):\n {0}".format(-translation_vector))  # in millimeters

    # project triade vectors endpoints to draw them on image
    # !! Z in triad points upwards, but axis positive direction points down
    triad = np.array([
        (0.0, 0.0, -100.0),  # z
        (100.0, 0.0, 0.0),  # x
        (0.0, 100.0, 0.0),  # y
    ])
    (endpoint2D, _) = cv2.projectPoints(triad, rotation_vector,
                                               translation_vector, camera_matrix, dist_coeffs)

    p1 = (int(image_points[0][0]), int(image_points[0][1]))

    # calculate coordinates for coordinate system triad vectors ends
    p2_z = (int(endpoint2D[0][0][0]), int(endpoint2D[0][0][1]))
    p2_x = (int(endpoint2D[1][0][0]), int(endpoint2D[1][0][1]))
    p2_y = (int(endpoint2D[2][0][0]), int(endpoint2D[2][0][1]))

    # draw triad
    cv2.line(canvas, p1, p2_z, (0, 0, 255), 20)  # red - z
    cv2.line(canvas, p1, p2_x, (0, 255, 0), 20)  # green - x
    cv2.line(canvas, p1, p2_y, (255, 0, 0), 20)  # blue - y

    canvas = cv2.putText(canvas, "Z", p2_z, cv2.FONT_HERSHEY_SIMPLEX, fontScale=3, color=(0, 0, 255), thickness=5)
    canvas = cv2.putText(canvas, "X", p2_x, cv2.FONT_HERSHEY_SIMPLEX, fontScale=3, color=(0, 255, 0), thickness=5)
    canvas = cv2.putText(canvas, "Y", p2_y, cv2.FONT_HERSHEY_SIMPLEX, fontScale=3, color=(255, 0, 0), thickness=5)

    view_image(canvas, "Contours")
